import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamWebsiteComponent } from './team-website/team-website.component';
import {TitleComponent} from './title/title.component';
import {AddPlayerComponent} from './add-player/add-player.component';

const routes: Routes = [
  { path: 'Home', component: TeamWebsiteComponent},
  { path: 'AddPlayer', component: AddPlayerComponent},
  { path: '', redirectTo: 'Home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
