import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { FantasyTeamService } from './fantasy-team.service';
import { TeamWebsiteComponent } from './team-website/team-website.component';
import { TitleComponent } from './title/title.component';
import { AddPlayerComponent } from './add-player/add-player.component';

@NgModule({
  declarations: [
    AppComponent,
    TeamWebsiteComponent,
    TitleComponent,
    AddPlayerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    FantasyTeamService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
