import { Component, OnInit } from '@angular/core';
import {FantasyTeamService} from '../fantasy-team.service';

@Component({
  selector: 'app-team-website',
  templateUrl: './team-website.component.html',
  styleUrls: ['./team-website.component.css']
})
export class TeamWebsiteComponent implements OnInit {

  // ***** Member Variables *****
  names: any[] = new Array(0);

  // ***** Constructors *****
  constructor(public fantasyService: FantasyTeamService) { }

  ngOnInit() {
    // Why do I have two of each item in my database??
    this.fantasyService.getAllItems().subscribe(teamInfo => { this.fantasyService.membersDatabase = teamInfo; });
  }

  // ***** Functions *****

  helloWorld() {
    console.log('Items: ' + this.fantasyService.membersDatabase[0].name);
    console.log('ALL NAMES: ' + this.getNames());
  }

  getNames(): any {
    for (let i = 0; i < this.fantasyService.membersDatabase.length; i++) {
      this.names[i] = this.fantasyService.membersDatabase[i].name;
    }
    return this.names;
  }

}
