import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamWebsiteComponent } from './team-website.component';

describe('TeamWebsiteComponent', () => {
  let component: TeamWebsiteComponent;
  let fixture: ComponentFixture<TeamWebsiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamWebsiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamWebsiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
