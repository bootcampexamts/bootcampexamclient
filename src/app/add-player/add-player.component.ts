import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup} from '@angular/forms';
import { FantasyTeamService } from '../fantasy-team.service';

@Component({
  selector: 'app-add-player',
  templateUrl: './add-player.component.html',
  styleUrls: ['./add-player.component.css']
})
export class AddPlayerComponent implements OnInit {

  tempName: any;
  tempNumber: any;
  tempPosition: any;
  tempDescription: any;
  newItem: FormGroup;

  constructor(private fantasyService: FantasyTeamService) { }

  ngOnInit() {
    this.newItem = new FormGroup({
      name: new FormControl(),
      number : new FormControl(),
      position: new FormControl(),
      description: new FormControl()
    });
  }

  storeItem(): void {
    this.tempName = this.newItem.controls.name.value;
    this.tempNumber = this.newItem.controls.number.value;
    this.tempPosition = this.newItem.controls.position.value;
    this.tempDescription = this.newItem.controls.description.value;
    console.log('You entered: ' + this.tempName);
  }

}
